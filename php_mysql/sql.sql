CREATE TABLE bowlers (
  bowlerid int primary key,
  name varchar(100),
  address varchar(200),
  phone varchar(20)
);

CREATE TABLE games (
  gameid int auto_increment primary key,
  bowlerid int,
  score int
);

INSERT INTO bowlers VALUES (100, 'Rich', '123 Main St.', '555-1234');
INSERT INTO bowlers VALUES (101, 'Barbara', '123 Main St.', '555-5678');
INSERT INTO bowlers VALUES (102, 'Katie Jane', '567 Oak St.', '555-0123');
INSERT INTO bowlers VALUES (103, 'Jessica', '901 Elm St.', '555-3256');

INSERT INTO games (bowlerid, score) VALUES (100, 110);
INSERT INTO games (bowlerid, score) VALUES (100, 115);
INSERT INTO games (bowlerid, score) VALUES (100, 105);
INSERT INTO games (bowlerid, score) VALUES (101, 110);
INSERT INTO games (bowlerid, score) VALUES (101, 112);
INSERT INTO games (bowlerid, score) VALUES (101, 130);
INSERT INTO games (bowlerid, score) VALUES (102, 115);
INSERT INTO games (bowlerid, score) VALUES (102, 125);
INSERT INTO games (bowlerid, score) VALUES (102, 140);
INSERT INTO games (bowlerid, score) VALUES (103, 135);
INSERT INTO games (bowlerid, score) VALUES (103, 138);
INSERT INTO games (bowlerid, score) VALUES (103, 130);